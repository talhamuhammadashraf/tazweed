import { Router } from "express";
import jwt from 'jsonwebtoken';
import {JWT_Secret} from '../config'
import UserModel from "../models/users";
import bcrypt from "bcrypt";
import Authorize from '../middleware/authJWT'
import {register,login} from '../services'
const AuthRouter = Router();
AuthRouter.post("/register",register);
AuthRouter.post('/login',login)
//to be removed
AuthRouter.post('/profile',Authorize
// (req,res,next)=>{
//     jwt.verify(req.body.token, JWT_Secret , function(err, decoded) {
//         if(decoded){
//             console.log(decoded)
//         }
//         else{console.log(err)}
//         next()
//       });
// }
,(req,res)=>{
    res.status(200).json({
        user:req.user
    })
})
export default AuthRouter;
