import {Router} from 'express';
import multer from 'multer';
import path from 'path'
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'fileuploads')
    },
    filename: function (req, file, cb) {
        let extension = () =>{
            if(file.mimetype == "image/gif"){
                return ".gif"
            }
            else if(file.mimetype == "image/png"){
                return ".png"
            }
            else if(file.mimetype == "image/jpeg"){return "jpg"}

        }
      cb(null, `${file.fieldname}${Date.now()}${extension()}`)
    }
  })
const Upload = multer({storage:storage})
const UploadRouter = Router();
UploadRouter.post('/upload',Upload.single("file"),(req,res)=>{
    res.send(path.join(`/uploadfile/file${Date.now()}.png`))
})

export default UploadRouter