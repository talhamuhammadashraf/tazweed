import jwt from "jsonwebtoken";
import { JWT_Secret } from "../config";
import UserModel from "../models/users";
export default (req, res, next) => {
  jwt.verify(req.body.token, JWT_Secret, function(err, decoded) {
    if (decoded) {
      const {email} = decoded;
      console.log(email,"email")
      UserModel.findOne({ email })
        .then(user => {
          req.user = user;
          console.log(user,"found in db")
          next()
        })
        .catch(error => {
          res.status(403).json({
            error: true,
            message: "invalid token"
          });
        });
    } else {
      res.status(403).json({
        error: true,
        message: "invalid token"
      });
    }
  });
};
