import mongoose from "mongoose";
var UserSchema = mongoose.Schema;

var UserModel = new UserSchema({
  name: { type: String, required: true },
  password: { type: String, required: true },
  email: { type: String, unique: true, required: true },
  // dob: { type: Date },
  phone: { type: Number }
});
export default mongoose.model("User", UserModel);
