import mongoose from "mongoose";
var UserSchema = mongoose.Schema;

var SlotsModel = new UserSchema({
  _id: mongoose.Types.ObjectId,
  from: { type: Number, required: true },
  to: { type: Number, required: true },
});
export default mongoose.model("User", SlotsModel);
