const  Port = process.env.Port || 8080;

const DB_Config = {
    connectionString: "mongodb://localhost/api"
  };

const JWT_Secret = "jwt$auth"
export {
    Port,
    DB_Config,
    JWT_Secret
}