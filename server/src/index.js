import express from "express";
import mongoose from "mongoose";
// import User from "./models/user";
// import { Register } from "./routers";
import bodyParser from "body-parser";
import cors from "cors";
import morgan from "morgan";
// import passport from "passport";
import path from "path";
import {DB_Config,Port} from "./config";
import { Users, Upload } from "./routes";
mongoose.connect(DB_Config.connectionString, { useNewUrlParser: true });
mongoose.set("useCreateIndex", true);
mongoose.connection.on("error", () =>
  console.log("error in connecting database")
);
mongoose.connection.once("open", () => console.log("db connected"));

const app = express();

app.use(cors({origin:"http://localhost:3000"}));

app.use('/fileuploads', express.static(path.join(__dirname, '/fileuploads')));

app.use(morgan("combined"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/user", Users);
app.use("/api", Upload);

app.all("/", (req, res) => res.status(404).send("invalid URL"));

app.listen(Port, () => console.log(`listening on ${Port}`));
