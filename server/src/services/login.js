import bcrypt from 'bcrypt';
import UserModel from '../models/users'
import jwt from "jsonwebtoken"
import {JWT_Secret} from '../config'
export default (req,res)=>{
    let { password, email } = req.body;
    if (!password || !email) {
      res.status(400).json({
        error: true,
        message: "please ensure all fields are filled"
      });
    }
    else{console.log('in else email pass found')
        UserModel.findOne({email}).then((user,error)=>{
            if(user){console.log("user found")
                bcrypt.compare(password,user.password,(error,isMatch)=>{
                    if(isMatch){
                        console.log("match");
                        const {
                          _id,
                          name,
                          email,
                          // dob,
                          phone
                        } = user;
                        const token =  jwt.sign({email},JWT_Secret)
                        res.status(200).json({
                          token,
                          error: false,
                          message: "success",
                          user: {
                            _id,
                            name,
                            email,
                            // dob,
                            phone
                          }
                        });
                    }
                    else{
                        console.log(error,"error in password")
                        res.status(400).json({error:true,message:"Password did not match"})
                    }

                })
            }
            else{
                res.status(404).json({error:true,message:`No user found with email ${email}`})
            }
        })
    }

}