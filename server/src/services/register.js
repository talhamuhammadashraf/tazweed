import UserModel from "../models/users";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken"
import { JWT_Secret } from '../config'
export default // function(req,res){
  (req, res) => {
    let { name, password, email, dob, phone } = req.body;
    if (!name || !password || !email || !dob || !phone) {
      res.status(400).json({
        error: true,
        message: "please ensure all fields are filled"
      });
    } else {
      UserModel.findOne({ email }, (error, user) => {
        if (user) {
          res
            .status(400)
            .json({ error: true, message: "this email is already in use" });
        } else {
          //   }
          // })
          bcrypt.hash(password, 10, (error, hashPass) => {
            if (!error) {
              password = hashPass;
              UserModel.create({
                name,
                password: hashPass,
                email,
                // dob,
                phone
              })
                .catch(error => {
                  res.status(500).json({
                    error: error,
                    message: "internal server error"
                  });
                })
                .then(() => {
                  const token = jwt.sign({ email }, JWT_Secret)
                  res.status(200).json({
                    token,
                    error: false,
                    message: "success",
                    user: {
                      name, email,
                      // dob,
                      phone
                    }
                  });
                });
            } else {
              res.status(500).json({
                error: error,
                message: "internal server error"
              });
            }
          });
        }
      });
    }
    //   }
  };
